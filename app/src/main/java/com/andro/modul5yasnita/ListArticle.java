package com.andro.modul5yasnita;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListArticle extends AppCompatActivity {
    Db_Resource dbHelper;
    RecyclerView recyclerView;
    ArrayList<Article> listArticle;
    AdapterArticle adapterArticle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setupSharedPreferences();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lists);
        getSupportActionBar().hide();
        dbHelper = new Db_Resource(getApplicationContext());
        listArticle = new ArrayList<>();
        recyclerView = findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(ListArticle.this));
        adapterArticle = new AdapterArticle(listArticle, ListArticle.this);
        recyclerView.setAdapter(adapterArticle);
        init();
    }
    private void setupSharedPreferences() {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        toggleTheme(prefs.getBoolean("nightMode", false));

    }

    public void toggleTheme(Boolean bo) {
        if (bo) {
            setTheme(R.style.dark);
        } else {
            setTheme(R.style.light);
        }

    }


    public void init() {
        listArticle.clear();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] projection = {
                Db_Resource.COLUMN_ID,
                Db_Resource.COLUMN_TITLE,
                Db_Resource.COLUMN_AUTHOR,
                Db_Resource.COLUMN_DESC,
                Db_Resource.COLUMN_DATE
        };

        Cursor cursor = db.query(
                Db_Resource.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );



        while (cursor.moveToNext()) {
            Toast.makeText(this, cursor.getString(cursor.getColumnIndex(Db_Resource.COLUMN_TITLE)), Toast.LENGTH_SHORT).show();
            listArticle.add(new Article(
                    cursor.getString(cursor.getColumnIndex(Db_Resource.COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(Db_Resource.COLUMN_TITLE)),
                    cursor.getString(cursor.getColumnIndex(Db_Resource.COLUMN_AUTHOR)),
                    cursor.getString(cursor.getColumnIndex(Db_Resource.COLUMN_DESC)),
                    cursor.getString(cursor.getColumnIndex(Db_Resource.COLUMN_DATE))
            ));
        }
        cursor.close();
        adapterArticle.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        dbHelper.close();
        super.onDestroy();
    }
}

