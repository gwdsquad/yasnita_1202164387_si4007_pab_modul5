package com.andro.modul5yasnita;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.andro.modul5yasnita.DetailActivity;
import com.andro.modul5yasnita.R;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;


public class AdapterArticle extends RecyclerView.Adapter<AdapterArticle.ViewHolder> {
    private ArrayList<Article> listArticle;
    private Context mContext;

    public AdapterArticle(ArrayList<Article> listArticle, Context mContext) {
        this.listArticle = listArticle;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AdapterArticle.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.menu_card, viewGroup, false)) {
        };
    }

    @Override
    public void onBindViewHolder(AdapterArticle.ViewHolder holder, int position) {
        Article article = listArticle.get(position);
        holder.bindTo(article);

    }
    public int getItemCount() {
        return listArticle.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView judul,author,desc;
        private String date,isi;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            SharedPreferences prefs = mContext.getSharedPreferences(mContext.getPackageName(), mContext.MODE_PRIVATE);

            judul = itemView.findViewById(R.id.txJudulCard);
            author = itemView.findViewById(R.id.txAuthor);
            desc = itemView.findViewById(R.id.txDesc);

            if (prefs.getBoolean("bigSize",false)){
                judul.setTextSize(itemView.getResources().getDimension(R.dimen.big)+14);
                author.setTextSize(itemView.getResources().getDimension(R.dimen.big)-10);
                judul.setTextSize(itemView.getResources().getDimension(R.dimen.big));
            }else{
                judul.setTextSize(itemView.getResources().getDimension(R.dimen.small)+14);
                author.setTextSize(itemView.getResources().getDimension(R.dimen.small)-12);
                judul.setTextSize(itemView.getResources().getDimension(R.dimen.small));
            }

            itemView.setOnClickListener(this);
        }

        @SuppressLint("StaticFieldLeak")
        public void bindTo(final Article article) {
            judul.setText(article.getTitle());
            author.setText(article.getAuthor());
            if (article.getDescription().length()>20){
                desc.setText((article.getDescription().substring(0,20)+"..."));
            }else {
                desc.setText(article.getDescription());
            }
            date = article.getCreated_at();
            isi = article.getDescription();
        }

        @Override
        public void onClick(View v) {
//            Toast.makeText(mContext, judul.getText().toString(), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(mContext, DetailActivity.class);
            intent.putExtra("judul",judul.getText().toString());
            intent.putExtra("isi",isi);
            intent.putExtra("date",date);
            intent.putExtra("author",author.getText().toString());
            mContext.startActivity(intent);
        }

    }
}

